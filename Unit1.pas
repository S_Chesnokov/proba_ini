unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore, dxSkinOffice2007Blue,
  PropFilerEh, PropStorageEh, cxTextEdit, Vcl.Menus, Vcl.StdCtrls, cxButtons;

type
  TForm1 = class(TForm)
    cxTextEdit1: TcxTextEdit;
    IniPropStorageManEh1: TIniPropStorageManEh;
    PropStorageEh1: TPropStorageEh;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    procedure cxButton1Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.cxButton1Click(Sender: TObject);
begin
PropStorageEh1.LoadProperties;
end;

procedure TForm1.cxButton2Click(Sender: TObject);
begin
PropStorageEh1.SaveProperties;
end;

end.
